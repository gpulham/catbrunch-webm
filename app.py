from flask import Flask, render_template, url_for, abort, session
from random import choice
import os
from urllib.parse import urljoin
from collections import deque


app = Flask(__name__)

if not os.path.exists('config.py'):
    with open('config.py', 'w') as f:
        key = os.urandom(24)
        f.write('SECRET_KEY = {}\n'.format(str(key)))

app.config.from_pyfile('config.py', silent=True)

@app.route('/<webm>', endpoint='webm')
def get_webm(webm):
    names = os.listdir(os.path.join(app.static_folder, 'webms'))
    webm_name = '{}.webm'.format(webm)

    if webm_name not in names:
        return abort(404)

    webm_path = url_for('static', filename=urljoin('webms/', webm_name))
    link = url_for('webm', webm=webm)
    return render_template('webm.html', webm=webm_path, name=webm, link=link)
    
@app.route('/')
@app.route('/shuffle', defaults={'shuffle': True})
def random(shuffle=False):
    names = os.listdir(os.path.join(app.static_folder, 'webms'))
    
    last_seen = []
    if 'last_seen' in session:
        last_seen = deque(session['last_seen'].split(';'), 4)
        names = [name for name in names if name not in last_seen]
    
    webm = choice(names)
        
    last_seen.append(webm)
    
    session['last_seen'] = ';'.join(last_seen)
    
    webm_path = url_for('static', filename=urljoin('webms/', webm))
    name, _ = os.path.splitext(os.path.basename(webm_path))
    link = url_for('webm', webm=name)
    
    return render_template('webm.html', webm=webm_path, name=name, link=link, shuffle=shuffle)
    
@app.errorhandler(404)
def page_not_found(e):
    return render_template('404.html'), 404
    
if __name__ == '__main__':
    app.run()
